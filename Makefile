build: static/css/style.css musl

.PHONY: musl
musl: target/x86_64-unknown-linux-musl/release/freeipa-self-service

.PHONY: run
run:
	./target/x86_64-unknown-linux-musl/release/freeipa-self-service

target/x86_64-unknown-linux-musl/release/freeipa-self-service:
	cargo build --release --target=x86_64-unknown-linux-musl --features vendored

static/css/style.css: vendor/bootstrap/scss
	sassc -t compressed -I vendor/bootstrap/scss -a src/scss/style.scss static/css/style.css

vendor/bootstrap/scss:
	git submodule init
	git submodule update

.PHONY: clean
clean:
	rm -rf static/css/style.css
	rm -rf target/x86_64-unknown-linux-musl/release/deps/freeipa_self_service*
	rm -rf target/x86_64-unknown-linux-musl/release/freeipa-self-service*
