#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;
extern crate serde_json;
extern crate rocket_contrib;
extern crate reqwest;

use serde_json::json;
use log::info;

// use std::string::String;
use std::path::{Path, PathBuf};

use rocket::fairing::AdHoc;
use rocket::request::{Form};
use rocket::response::{Redirect, status, NamedFile};
use rocket::http::Status;
use rocket::State;

use rocket_contrib::templates::{Template};

#[derive(Serialize)]
struct TemplateContext {
    title: &'static str,
    // This key tells handlebars which template is the parent.
    parent: &'static str,
}

#[derive(Serialize)]
struct RegisteredContext {
    title: &'static str,
    message: String,
    // This key tells handlebars which template is the parent.
    parent: &'static str,
}

#[derive(FromForm)]
struct RegisterData {
    givenname: String,
    sn: String, 
    pass: String,
    email: String,
    username: String,
}

#[post("/api/register", data = "<register>")]
fn apiregister(register: Form<RegisterData>, config: State<ConfigState>) -> Result<Redirect, status::Custom<String>> {
    let request_url = format!("{}/ipa/session/json", config.ipa_host);
    info!("{}", request_url);
    
    let json = json!({
        "id": 0, 
        "method": "stageuser_add", 
        "params": [
            [
                format!("{}", register.username)
            ], 
            {
                "givenname": format!("{}", register.givenname),
                "mail": [
                    format!("{}", register.email)
                ],
                "sn": format!("{}", register.sn),
                "userpassword": format!("{}", register.pass),
                "version": format!("{}", config.ipa_api_version)
            }
        ]
    });
  
    let client = reqwest::Client::builder()
        .cookie_store(true)
        .danger_accept_invalid_certs(config.disable_ssl_verification)
        .build()
        .unwrap();

    let login_request_url = format!("{}/ipa/session/login_password", config.ipa_host);
    let params = [("user", &config.ipa_user), ("password", &config.ipa_pass)];
    return match client
        .post(&login_request_url)
        .form(&params)
        .send() {
        Err(e) => Err(status::Custom(Status::InternalServerError, format!("Failed Parsing Response, '{}'.", e))),
        Ok(_) => {
            return match client
                .post(&request_url)
                .header(reqwest::header::REFERER, format!("{}/ipa", config.ipa_host))
                .json(&json)
                .send() {
                Err(e) => Err(status::Custom(Status::InternalServerError, format!("Failed Parsing Response, '{}'.", e))),
                Ok(mut res) => {
                    return match res.status() {
                        reqwest::StatusCode::OK => {
                            return match res.text() {
                                Err(e) => Err(status::Custom(Status::InternalServerError, format!("Failed Parsing Response, '{}'.", e))),
                                Ok(text) => {
                                    return match serde_json::from_str::<serde_json::Value>(&text) {
                                        Err(e) => Err(status::Custom(Status::InternalServerError, format!("Failed Parsing Response, '{}'.", e))),
                                        Ok(_) => {
                                            Ok(Redirect::to("/registered"))
                                        }
                                    }
                                }
                            };
                        },
                        s => {
                            Err(status::Custom(Status::InternalServerError, format!("Failed Parsing Response, '{}'.", s)))
                        }
                    };
                }
            }
        }
    };
}

#[get("/")]
fn register() -> Template {
    Template::render("register", &TemplateContext {
        title: "Register",
        parent: "layout"
    })
}

#[get("/registered")]
fn registered(config: State<ConfigState>) -> Template {
    Template::render("registered", &RegisteredContext {
        title: "Register",
        message: config.registered_message.to_owned(),
        parent: "layout"
    })
}

struct ConfigState {
    ipa_host: String,
    ipa_user: String,
    ipa_pass: String,
    ipa_api_version: String,
    registered_message: String,
    static_dir: String,
    disable_ssl_verification: bool,
}

#[get("/static/<asset..>")]
fn assets(asset: PathBuf, config: State<ConfigState>) -> Option<NamedFile> {
    NamedFile::open(Path::new(&config.static_dir).join(asset)).ok()
}

fn main() {
    rocket::ignite()
        .mount("/", routes![register,apiregister,registered,assets])
        // .attach(Template::custom(|engines| {
            // engines.handlebars.register_helper("wow", Box::new(wow_helper));
        // }))
        .attach(Template::fairing())
        .attach(AdHoc::on_attach("Set Config", |rocket| {
            println!("Adding config to managed state...");
            let ipa_host = rocket.config().get_string("ipa_host").unwrap();
            let ipa_user = rocket.config().get_string("ipa_user").unwrap();
            let ipa_pass = rocket.config().get_string("ipa_pass").unwrap();
            let ipa_api_version = rocket.config().get_string("ipa_api_version").unwrap_or("2.230".to_string());
            let registered_message = rocket.config().get_string("registered_message").unwrap_or("".to_string());
            let static_dir = rocket.config().get_str("static_dir").unwrap_or("static/").to_string();
            let disable_ssl_verification = rocket.config().get_bool("disable_ssl_verification").unwrap_or(false);
            Ok(rocket.manage(ConfigState {ipa_host: ipa_host, ipa_user: ipa_user, ipa_pass: ipa_pass, ipa_api_version: ipa_api_version, registered_message: registered_message, static_dir: static_dir, disable_ssl_verification: disable_ssl_verification}))
        }))
        .launch();
}
