# ------------------------------------------------------------------------------
# Cargo Build Stage
# ------------------------------------------------------------------------------

FROM rust:latest as cargo-build

RUN apt-get update

RUN apt-get install musl-tools -y

RUN rustup default nightly && rustup update
RUN rustup target add x86_64-unknown-linux-musl --toolchain=nightly

WORKDIR /work

COPY . .

RUN cargo build --release --target=x86_64-unknown-linux-musl --features vendored

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------

FROM alpine:latest

COPY --from=cargo-build /work/target/x86_64-unknown-linux-musl/release/freeipa-self-service /
COPY --from=cargo-build /work/templates /templates
COPY --from=cargo-build /work/static /static

ENV ROCKET_TEMPLATE_DIR /templates
ENV ROCKET_STATIC_DIR /static

RUN apk add openssl
RUN adduser selfservice -D

USER selfservice

CMD ["/freeipa-self-service"]
